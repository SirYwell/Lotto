package de.crafttopia.lotto.commands;

import de.crafttopia.lotto.Main;
import de.crafttopia.lotto.config.ConfigManager;
import de.crafttopia.lotto.mysql.MySQL;
import de.crafttopia.lotto.uuidfetcher.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by Benedikt on 21.07.2017.
 */
public class CommandLotto extends Command {

    public CommandLotto() {
        super("lotto", "/lotto <addpot/removepot> <amount>", "lotto.use");
    }

    public boolean execute(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Please issue this command as a player.");
            //console cannot participate. at all
            return true;
        }
        Player p = (Player) sender;
        if (args.length == 0) {
            //they want some info
            try {
                /*Calculating the current jackpot via amount of tickets, the price for a single ticket and the additional jackpot value*/
                double current_jackpot = Main.config.getDouble("ticket-price") * MySQL.executeQueries("select id from lotto_current_game_tickets", "id").size();
                current_jackpot += (double) MySQL.executeQuery("select added_pot from lotto_current_game_props where id = 0", "added_pot");

                int player_count = MySQL.executeQueries("select distinct uuid from lotto_current_game_tickets", "uuid").size();

                int ticket_count = MySQL.executeQueries("select uuid from lotto_current_game_tickets", "uuid").size();

                int tickets_bought = MySQL.executeQueries("select uuid from lotto_current_game_tickets where uuid = '" + p.getUniqueId().toString() + "'", "uuid").size();

                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                Date draw_time = new Date((long) MySQL.executeQuery("select draw_time from lotto_current_game_props", "draw_time"));


                String lastWinnerName = null;
                Object o =MySQL.executeQuery("select uuid from lotto_winners order by id desc limit 1", "uuid");
                if(o != null) {
                    lastWinnerName = UUIDFetcher.getName(UUID.fromString((String)o));
                }
                double lastWinnerAmount = 0.0d;

                if(lastWinnerName != null) {
                    lastWinnerAmount = (double) MySQL.executeQuery("select amount_won from lotto_winners order by id desc limit 1", "amount_won");
                }

                // calculate chance of winning
                double chance = (double) tickets_bought / (double) ticket_count;
                if(Double.isNaN(chance)) {
                    chance = 0;
                }

                //they get some info
                p.sendMessage(ConfigManager.getString("message.status")
                        .replaceAll("<jackpot>", formatReadable(current_jackpot))
                        .replaceAll("<players>", String.valueOf(player_count))
                        .replaceAll("<tickets_total>", String.valueOf(ticket_count))
                        .replaceAll("<time>", sdf.format(draw_time))
                        .replaceAll("<ticket_price>", formatReadable(Main.config.getDouble("ticket-price")))
                        .replaceAll("<tickets_bought>", String.valueOf(tickets_bought))
                        .replaceAll("<win_percentage>", formatReadable(chance * 100))
                        .replaceAll("<last_winner>", lastWinnerName == null ? "---" : lastWinnerName)
                        .replaceAll("<last_winner_amount>", formatReadable(lastWinnerAmount)));
            } catch (Exception e) {
                //please don't
                e.printStackTrace();
            }
            return true;
        } else {
            //arguments are equal to or greater than 1 let's be honest who cares
            Pattern number = Pattern.compile("[1-9]+[0-9]*");
            //but the first argument has to be a number however
            if (number.matcher(args[0]).matches()) {
                int amount = Integer.parseInt(args[0]);
                int max_amount = Main.config.getInt("max-tickets");
                if (amount > max_amount) {
                    p.sendMessage(ConfigManager.getString("message.ticketlimit").replaceAll("<amount>", String.valueOf(max_amount)));
                    return true;
                }
                try {
                    int bought_amount = MySQL.executeQueries("select uuid from lotto_current_game_tickets where uuid = '" + p.getUniqueId().toString() + "'", "uuid").size();
                    if (bought_amount >= max_amount) {
                        p.sendMessage(ConfigManager.getString("message.maxtickets").replaceAll("<amount>", String.valueOf(max_amount)));
                        return true;
                    }
                    if (bought_amount + amount > max_amount) {
                        p.sendMessage(ConfigManager.getString("message.ticket-buy-left").replaceAll("<amount>", String.valueOf(max_amount - bought_amount)));
                        return true;
                    }
                    double cost = amount * Main.config.getDouble("ticket-price");
                    if (!Main.economy.has(p, cost)) {
                        p.sendMessage(ConfigManager.getString("message.insufficient-funds"));
                        return true;
                    }
                    Main.economy.withdrawPlayer(p, cost);
                    for (int i = 0; i < amount; i++) {
                        MySQL.executeUpdate("insert into lotto_current_game_tickets (`id` , `uuid`) values (NULL , '" + p.getUniqueId().toString() + "')");
                    }
                    double current_jackpot = Main.config.getDouble("ticket-price") * MySQL.executeQueries("select id from lotto_current_game_tickets", "id").size();
                    current_jackpot += (double) MySQL.executeQuery("select added_pot from lotto_current_game_props where id = 0", "added_pot");
                    Bukkit.broadcastMessage(ConfigManager.getString("message.broadcast-buy")
                            .replaceAll("<player>", p.getName()).replaceAll("<amount>", String.valueOf(amount))
                            .replaceAll("<g_ticket>", amount == 1 ? (Main.config.getString("message.ticket.singular")) : (Main.config.getString("message.ticket.plural")))
                            .replaceAll("<jackpot>", formatReadable(current_jackpot)));
                    p.sendMessage(ConfigManager.getString("message.notify-bought").replaceAll("<amount>", String.valueOf(amount))
                            .replaceAll("<g_ticket>", amount == 1 ? (Main.config.getString("message.ticket.singular")) : (Main.config.getString("message.ticket.plural")))
                            .replaceAll("<cost>", formatReadable(cost)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                //but if you're cool enough you can also enter some admin commands instead of integers
                if (args[0].equalsIgnoreCase("addpot") || args[0].equalsIgnoreCase("removepot")) {
                    if (!(sender.hasPermission("lotto.admin"))) {
                        //but if you're not you will be rejected
                        sender.sendMessage(ConfigManager.getString("nopermission"));
                        return true;
                    }
                    if(args.length == 1) {
                        //how could you mess this up
                        return false;
                    }
                    Pattern dble = Pattern.compile("[0-9]+(\\.[0-9]+)?");
                    //you may enter a decimal value.
                    if (!dble.matcher(args[1]).matches()) {
                        //...
                        p.sendMessage("double-format");
                        return true;
                    }
                    try {
                        //update the database with the new value

                        //ternary expression, if you don't add, it's likely you subtract
                        MySQL.executeUpdate("update lotto_current_game_props set added_pot = added_pot " +
                                (args[0].equalsIgnoreCase("addpot") ? "+" : "-") + " " + Double.parseDouble(args[1]));

                        /*Calculate the jackpot via amount of tickets, price for a single ticket and additional jackpot value*/
                        double current_jackpot = Main.config.getDouble("ticket-price") * MySQL.executeQueries("select id from lotto_current_game_tickets", "id").size();
                        current_jackpot += (double) MySQL.executeQuery("select added_pot from lotto_current_game_props where id = 0", "added_pot");

                        //inform the CommandSender about what he has done
                        p.sendMessage(ConfigManager.getString(args[0].equalsIgnoreCase("addpot") ? "message.addedpot" : "message.removedpot")
                                .replaceAll("<jackpot>", formatReadable(current_jackpot)).replaceAll("<amount>", args[1]));

                        //and also tell everyone
                        Bukkit.broadcastMessage(ConfigManager.getString("message.broadcast-change").replaceAll("<jackpot>", formatReadable(current_jackpot)));
                    } catch (Exception e) {
                        sender.sendMessage(ConfigManager.getString("error"));
                        e.printStackTrace();
                    }
                    return true;
                } else {
                    //you failed me
                    sender.sendMessage(ConfigManager.getString("number-format"));
                    return true;
                }
            }
        }
        return true;
    }

    private String formatReadable(double d) {
        return String.format(Locale.getDefault(), "%.2f", d);
    }

}
