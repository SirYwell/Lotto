package de.crafttopia.lotto.game;

import de.crafttopia.lotto.config.ConfigManager;
import de.crafttopia.lotto.Main;
import de.crafttopia.lotto.mysql.MySQL;
import de.crafttopia.lotto.uuidfetcher.UUIDFetcher;
import org.bukkit.Bukkit;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Benedikt on 21.07.2017.
 */
public class Game {

    public Game() {
        try {
            /*If there is no current game running*/
            if (MySQL.executeQuery("select id from lotto_current_game_props where id = 0", "id") == null) {
                /*create one*/

                //Will be used later to schedule the tasks
                List<String> times = Main.config.getStringList("draw-times");

                //Will be used later to ease the creation of unix dates
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

                //with this line we get "0th" hour of the day (e.g. it's 12-07-2017 18:19 will result in 12-07-2017 00:00)
                //maybe i could've done this easier
                long time_zero = dateFormat.parse(dateFormat.format(new Date())).getTime();
                long time_now = new Date().getTime();

                //Initialize the date that will be used in the database with null
                Date toUse = null;

                //the list with the draw times cannot be empty
                if (times == null || times.isEmpty()) {
                    throw new IllegalArgumentException("Lottery times cannot be empty! Please setup your de.crafttopia.lotto.config properly!");
                }

                //search for the next viable draw time
                while (toUse == null) {
                    //in every time
                    for (String time : times) {
                        //read the time from config (which is given e.g. like 15:00 but is actually worth 14h as unix time starts at 01-01-1970 01:00
                        long add_time = 60 * 60 * 1000 + sdf.parse(time).getTime(); //so adding an hour will fix it
                        if (time_zero + add_time > time_now) { //if this calculated time is greater then the time it is now, this is the time of the next drawing
                            toUse = new Date(time_zero + add_time);
                            break; //set the date and exit
                        }
                    }
                    time_zero += 1000 * 60 * 60 * 24; //the next drawing will be tomorrow
                }

                //insert the game into MySQL
                MySQL.executeUpdate("insert into lotto_current_game_props (`id`, `added_pot`, `draw_time`) values " +
                        "(0, " + Main.config.getDouble("pot-start") + ", " + toUse.getTime() + ")");

                //And tell everyone who is online when the next game will start
                Bukkit.broadcastMessage(ConfigManager.getString("message.remind-next-start").replaceAll("<date>", dateFormat.format(toUse))
                        .replaceAll("<time>", sdf.format(toUse)));
            }


            //if we have a game, but is already expired, we delete it and go to the start of this constructor
            long draw_time = (long) MySQL.executeQuery("select draw_time from lotto_current_game_props where id = 0", "draw_time");

            if (draw_time < new Date().getTime()) {
                MySQL.executeUpdate("delete from lotto_current_game_props");
                new Game();
                return;
            }

            //now we schedule the delayed tasks that tell us how much time is left until the next drawing
            List<Integer> remind_times = Main.config.getIntegerList("reminder-times");

            long draw_in_ticks = ((draw_time - new Date().getTime()) / 1000) * 20;
            for (Integer i : remind_times) {
                long t = draw_in_ticks - (i * 20 * 60);
                if (t < 0) {
                    continue;
                }
                Bukkit.getScheduler().runTaskLater(Main.main, new RemindTask(), t);
            }
            Bukkit.getScheduler().runTaskLater(Main.main, new DrawTask(), draw_in_ticks);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class RemindTask implements Runnable {

        @Override
        public void run() {
            try {
                //This will tell us how much time (in minutes) is left until the next winner will be drawn
                long draw_time = (long) MySQL.executeQuery("select draw_time from lotto_current_game_props where id = 0", "draw_time");
                long draw_in_ticks = ((draw_time - new Date().getTime()) / 1000) * 20;
                double current_jackpot = Main.config.getDouble("ticket-price") * MySQL.executeQueries("select id from lotto_current_game_tickets", "id").size();

                current_jackpot += (double) MySQL.executeQuery("select added_pot from lotto_current_game_props where id = 0", "added_pot");

                String minutes = String.valueOf((int) (draw_in_ticks / 20) / 60 + 1);

                Bukkit.broadcastMessage(ConfigManager.getString("message.remind-start").replaceAll("<time>", minutes)
                        .replaceAll("<jackpot>", String.valueOf(current_jackpot)));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    class DrawTask implements Runnable {

        @Override
        public void run() {
            try {
                //get every ticket with it's owner from the database
                ArrayList<Object> uuids = MySQL.executeQueries("select uuid from lotto_current_game_tickets", "uuid");

                //but we can only play, if players have entered... obviously
                if (!uuids.isEmpty()) {

                    //for more pseudo-randomness, shuffle this
                    Collections.shuffle(uuids, new Random(System.nanoTime()));

                    //and select one at random
                    UUID winner = UUID.fromString((String) uuids.get(new Random(System.nanoTime()).nextInt(uuids.size())));

                    //calulate the current jackpot by the count of tickets, price per ticket and additional pot
                    double current_jackpot = Main.config.getDouble("ticket-price") * MySQL.executeQueries("select id from lotto_current_game_tickets", "id").size();

                    //add additional pot to jackpot
                    current_jackpot += (double) MySQL.executeQuery("select added_pot from lotto_current_game_props where id = 0", "added_pot");

                    //announce winner
                    Bukkit.broadcastMessage(ConfigManager.getString("message.winner").replaceAll("<player>", UUIDFetcher.getName(winner))
                            .replaceAll("<jackpot>", String.valueOf(current_jackpot)));

                    //we don't wanna scam anyone do we
                    Main.economy.depositPlayer(Bukkit.getOfflinePlayer(winner), current_jackpot);

                    //but if they are online we can tell them right away
                    if (Bukkit.getOfflinePlayer(winner).isOnline()) {
                        Bukkit.getPlayer(winner).sendMessage(ConfigManager.getString("message.won").replaceAll("<jackpot>", String.valueOf(current_jackpot)));
                    }

                    //and we delete the tickets and the game properties
                    MySQL.executeUpdate("delete from lotto_current_game_props");
                    MySQL.executeUpdate("delete from lotto_current_game_tickets");
                    MySQL.executeUpdate("insert into lotto_winners (`id`, `uuid`, `amount_won`) values (NULL, '" + winner.toString() + "', " + current_jackpot + ")");

                    //start a new game with a delay of 3 seconds (for style)
                    Bukkit.getScheduler().runTaskLater(Main.main, Game::new, 60L);
                } else {
                    //noone came to play :(
                    Bukkit.broadcastMessage(ConfigManager.getString("message.noplayers"));
                }
            } catch (Exception e) {
                //oh no
                e.printStackTrace();
            }
        }

    }

}
